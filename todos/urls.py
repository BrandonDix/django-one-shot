from django.urls import path
from todos.views import (
    TodoListView, 
    TodoDetailView, 
    TodoCreateView, 
    TodoDeleteView, 
    TodoUpdateView, 
    TodoUpdateView, 
    TodoItemCreateView, 
    TodoItemUpdateView,
)
urlpatterns = [
    path("", TodoListView.as_view(), name = "todos_list"),
    path("<int:pk>/detail", TodoDetailView.as_view(), name = "todos_detail"),
    path("create/", TodoCreateView.as_view(), name = "todos_create"),
    path("<int:pk>/delete", TodoDeleteView.as_view(), name = "todos_delete"),
    path("<int:pk>/edit", TodoUpdateView.as_view(), name = "todos_update"),
    path("items/create/", TodoItemCreateView.as_view(), name = "todos_item_create"),
    path("items/<int:pk>/edit", TodoItemUpdateView.as_view(), name = "todos_item_update"),
]