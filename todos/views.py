from dataclasses import fields
from email.policy import HTTP
from django.shortcuts import render
from django.urls import reverse_lazy

from todos.models import TodoItem, TodoList
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView


#Lists

class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todos_table"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    success_url = reverse_lazy("todos_list")

class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])

class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"

    success_url = reverse_lazy("todos_list")





#Items

class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos_items/create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    success_url = reverse_lazy("todos_list")

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos_items/update.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])